<?xml version="1.0" ?>

<!DOCTYPE module SYSTEM "helix-build.dtd">

<module>
    
    <!-- Defaults for this package -->
    <packsys id="default">
        <os id="default">
            <osvers id="default">
                <arch id="default">
                    <rcsid>$Id$</rcsid>
                    <name>libxslt</name>
		    <version>1.1.10</version>
		    <rev>0</rev>
		    <serial>1</serial>

		    <source><i>CVS :pserver:{{GNOMECVS_USER}}@cvs.gnome.org:/cvs/gnome [[name]] HEAD</i></source>

                    <build id="default">
                        <prepare>[[snapconfigure]]</prepare>
                        <compile>[[snapmake]]</compile>
                        <install>[[snapinstall]]; rm -rf ${DESTDIR}[[snapprefix]]/doc</install>
			<package id="default">
			    <name>libxslt-snapshot</name>
			    <files>
				<i>[[snapprefix]]/bin/xsltproc</i>
				<i>[[snapprefix]]/lib/lib*.[[so]].*</i>
				<i>[[snapprefix]]/lib/python*/site-packages/*.py</i>
				<i>[[snapprefix]]/lib/python*/site-packages/*.[[so]]</i>
				<i>[[snapmandir]]/man1/*</i>
			    </files>
			    <docs>
				<i>AUTHORS</i>
				<i>ChangeLog</i>
				<i>NEWS</i>
				<i>README</i>
				<i>Copyright</i>
				<i>TODO</i>
			    </docs>
			    <script id="postinst">
				<i>[[ldconfig]]</i>
			    </script>
			    <script id="postrm">
				<i>[[ldconfig]]</i>
			    </script>
			    <description>
<h>Library providing XSLT support</h>
<p>This C library lets you transform XML files into other XML files (or
HTML, text, etc.) using the standard XSLT stylesheet transformation
mechanism. To use it, you need to have a version of libxml2 >= 2.3.8
installed.</p>
			    </description>
			</package>
			<package id="devel">
			    <name>libxslt-snapshot-devel</name>
			    <files>
				<i>[[snapprefix]]/bin/xslt-config</i>
				<i>[[snapprefix]]/include/*</i>
				<i>[[snapprefix]]/lib/lib*.[[so]]</i>
				<i>[[snapprefix]]/lib/*.a</i>
				<i>[[snapprefix]]/lib/*.la</i>
				<i>[[snapprefix]]/lib/*.sh</i>
				<i>[[snapprefix]]/lib/pkgconfig/*.pc</i>
				<i>[[snapprefix]]/lib/python*/site-packages/*.a</i>
				<i>[[snapprefix]]/lib/python*/site-packages/*.la</i>
				<i>[[snapmandir]]/man3/*</i>
				<i>[[snapprefix]]/share/aclocal/*</i>
				<i>[[snapprefix]]/share/doc/*</i>
			    </files>
			    <description>
<h>Libraries, includes, etc. to develop XSLT applications</h>
<p>This C library lets you transform XML files into other XML files (or
HTML, text, etc.) using the standard XSLT stylesheet transformation
mechanism. To use it you need to have a version of libxml2 >= 2.3.8
installed.</p>
			    </description>
			</package>
		    </build>
                </arch>
            </osvers>
        </os>
    </packsys>

    <packsys id="rpm">
	<os id="default">
	    <osvers id="default">
		<arch id="default">
		    <psdata id="url">http://xmlsoft.org/XSLT/</psdata>
		    <psdata id="copyright">MIT</psdata>
		    <build id="default">
			<builddep id="buildrequires">
			    <i>libxml2-snapshot-devel &gt;= 2.6.8</i>
			    <i>python-devel</i>
			    <i>zlib-devel</i>
			</builddep>
			<package id="default">
			    <psdata id="group">System Environment/Libraries</psdata>
			    <dep id="requires">
				<i>libxml2-snapshot &gt;= 2.6.8</i>
			    </dep>
			    <dep id="conflicts">
				<i>[[name]]-snapshot-devel &lt; [[fullversion]]</i>
				<i>[[name]]-snapshot-devel &gt; [[fullversion]]</i>
			    </dep>
			</package>
			<package id="devel">
			    <psdata id="group">Development/Libraries</psdata>
			    <dep id="requires">
				<i>libxml2-snapshot-devel &gt;= 2.6.8</i>
				<i>[[name]]-snapshot = [[fullversion]]</i>
			    </dep>
			</package>
		    </build>
		</arch>
	    </osvers>
	</os>
    </packsys>
    <packsys id="dpkg">
        <os id="default">
            <osvers id="default">
                <arch id="default">
                    <srcname>libxslt</srcname>
                    <psdata id="section">text</psdata>
                    <psdata id="priority">optional</psdata>

                    <build id="default">
                        <install>${MAKE} DESTDIR=${DESTDIR} install; rm -rf ${DESTDIR}[[snapprefix]]/doc; install -m 644 -D debian/xslt-config.1 ${DESTDIR}[[snapmandir]]/man1/xslt-config.1</install>
			<builddep id="build-depends">
			    <i>debhelper</i>
			    <i>libz-dev</i>
			    <i>libxml2-snapshot-dev</i>
			</builddep>
                        <package id="default">
                            <name>libxslt-snapshot</name>
                            <psdata id="section">libs</psdata>
                            <psdata id="architecture">any</psdata>
			    <changelog>ChangeLog</changelog>
                            <dep id="depends">
                                <i>${shlibs:Depends}</i>
                            </dep>
			    <docs>
				<i>AUTHORS</i>
				<i>NEWS</i>
				<i>README</i>
				<i>Copyright</i>
				<i>TODO</i>
			    </docs>
                            <script id="postinst"/>
			    <script id="postrm"/>
                        </package>
                        <package id="devel">
                            <name>libxslt-snapshot-dev</name>
                            <psdata id="section">devel</psdata>
                            <psdata id="architecture">any</psdata>
                            <dep id="depends">
				<i>libxslt-snapshot (= ${Source-Version})</i>
                                <i>libxml2-snapshot-dev</i>
                            </dep>
                        </package>
                    </build>
                </arch>
            </osvers>
        </os>
    </packsys>
</module>

<!--
Local Variables:
mode: xml
End:
-->
